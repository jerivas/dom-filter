(function webpackUniversalModuleDefinition(root, factory) {
	if(typeof exports === 'object' && typeof module === 'object')
		module.exports = factory(require("fuse.js"));
	else if(typeof define === 'function' && define.amd)
		define(["fuse.js"], factory);
	else if(typeof exports === 'object')
		exports["DOMFilter"] = factory(require("fuse.js"));
	else
		root["DOMFilter"] = factory(root["Fuse"]);
})(this, function(__WEBPACK_EXTERNAL_MODULE_0__) {
return /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;
/******/
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// identity function for calling harmony imports with the correct context
/******/ 	__webpack_require__.i = function(value) { return value; };
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = __WEBPACK_EXTERNAL_MODULE_0__;

/***/ }),
/* 1 */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_fuse_js__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_fuse_js___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_fuse_js__);
var _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; };

function _toConsumableArray(arr) { if (Array.isArray(arr)) { for (var i = 0, arr2 = Array(arr.length); i < arr.length; i++) { arr2[i] = arr[i]; } return arr2; } else { return Array.from(arr); } }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }



/**
 * Converts a DOM node into an Object
 * @param  {HTMLElement} el The DOM element to be serialized
 * @return {Object}         Contains a single 'content' key with the DOM text
 */
var simpleSerializer = function simpleSerializer(el) {
	return { content: el.textContent.replace(/\s{2,}/g, ' ') };
};

/**
 * Configures Fuse to search for a 'content' key, as returned by simpleSerializer
 * @type {Object}
 */
var defaultFuseConfig = {
	shouldSort: true,
	keys: ['content']
};

var DOMFilter =

/**
 * Configure DOMFilter
 *
 * @param  {NodeList} options.items        DOM elements to be filtered
 * @param  {Object} [options.fuseConfig]   Config object passed directly to fuse.js
 * @param  {Function} [options.serializer]
 *         Serializer function. Must take a DOM node as single parameter and return
 *         an Object as the serialized representation of the DOM node.
 *
 * @param  {String} [options.itemMatchClass]
 *         Class applied to matching items
 * @param  {String} [options.itemNotMatchClass]
 *         Class applied to non-matching items
 * @param  {String} [options.containerMatchClass]
 *         Class applied to the parent when at least one match exists
 * @param  {String} [options.containerNotMatchClass]
 *         Class applied to the parent when no matches exist
 *
 * @return {Object}                        A new DOMFilter instance
 */
function DOMFilter(_ref) {
	var items = _ref.items,
	    _ref$fuseConfig = _ref.fuseConfig,
	    fuseConfig = _ref$fuseConfig === undefined ? defaultFuseConfig : _ref$fuseConfig,
	    _ref$serializer = _ref.serializer,
	    serializer = _ref$serializer === undefined ? simpleSerializer : _ref$serializer,
	    _ref$itemMatchClass = _ref.itemMatchClass,
	    itemMatchClass = _ref$itemMatchClass === undefined ? 'is-match' : _ref$itemMatchClass,
	    _ref$itemNotMatchClas = _ref.itemNotMatchClass,
	    itemNotMatchClass = _ref$itemNotMatchClas === undefined ? 'is-not-match' : _ref$itemNotMatchClas,
	    _ref$containerMatchCl = _ref.containerMatchClass,
	    containerMatchClass = _ref$containerMatchCl === undefined ? 'has-match' : _ref$containerMatchCl,
	    _ref$containerNotMatc = _ref.containerNotMatchClass,
	    containerNotMatchClass = _ref$containerNotMatc === undefined ? 'has-no-match' : _ref$containerNotMatc;

	_classCallCheck(this, DOMFilter);

	_initialiseProps.call(this);

	this.items = [].concat(_toConsumableArray(items)); // Cast to array
	this.serializer = serializer;
	this.fuseConfig = fuseConfig;

	this.itemMatchClass = itemMatchClass;
	this.itemNotMatchClass = itemNotMatchClass;
	this.containerMatchClass = containerMatchClass;
	this.containerNotMatchClass = containerNotMatchClass;

	this.parentNode = this.items[0].parentNode;
	this.serializedItems = this.serialize();
	this.fuse = new __WEBPACK_IMPORTED_MODULE_0_fuse_js___default.a(this.serializedItems, this.fuseConfig);

	return this;
}

/**
 * Apply the serializer function on each item. Each serialized Object will have an
 * 'index' key with its original DOM position plus any keys found in the Object
 * returned by the serializer function.
 * @return {Array}               Array of serialized Objects
 */


/**
 * Search and filter the DOM items.
 * Filtering will re-order the DOM elements by relevance, and apply CSS classes
 * to indicate which elements match the query. The parent of the items will also
 * receive classes to indicate if matching elements are present or not.
 * @param  {String} query The search query
 * @return {Number}       The number of matching elements
 */
;

var _initialiseProps = function _initialiseProps() {
	var _this = this;

	this.serialize = function () {
		return _this.items.map(function (el, i) {
			return _extends({
				index: i
			}, _this.serializer(el));
		});
	};

	this.filter = function (query) {
		var shouldFilter = query.length > 0;
		var items = _this.items,
		    parentNode = _this.parentNode,
		    itemMatchClass = _this.itemMatchClass,
		    itemNotMatchClass = _this.itemNotMatchClass,
		    containerMatchClass = _this.containerMatchClass,
		    containerNotMatchClass = _this.containerNotMatchClass;

		// Reset all item and container classes

		items.forEach(function (item) {
			if (shouldFilter) {
				item.classList.add(itemNotMatchClass);
				item.classList.remove(itemMatchClass);
			} else {
				item.classList.add(itemMatchClass);
				item.classList.remove(itemNotMatchClass);
				parentNode.classList.add(containerMatchClass);
				parentNode.classList.remove(containerNotMatchClass);
			}

			// Also reset the DOM order
			parentNode.appendChild(item);
		});

		// Exit early if no filtering needs to happen
		if (!shouldFilter) return items.length;

		// Reverse the results so DOM insertion works as expected
		var results = _this.fuse.search(query).reverse();
		results.forEach(function (result) {
			var item = items[result.index];

			// Apply match classes on the results
			item.classList.add(itemMatchClass);
			item.classList.remove(itemNotMatchClass);

			// Make DOM order match with result order
			parentNode.insertBefore(item, parentNode.firstChild);
		});

		if (results.length > 0) {
			parentNode.classList.add(containerMatchClass);
			parentNode.classList.remove(containerNotMatchClass);
		} else {
			parentNode.classList.add(containerNotMatchClass);
			parentNode.classList.remove(containerMatchClass);
		}

		return results.length;
	};
};

/* harmony default export */ __webpack_exports__["default"] = (DOMFilter);

/***/ })
/******/ ]);
});
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay91bml2ZXJzYWxNb2R1bGVEZWZpbml0aW9uIiwid2VicGFjazovLy93ZWJwYWNrL2Jvb3RzdHJhcCA0Y2E3ZWNhMjkxNGFhZjhhN2Y2MiIsIndlYnBhY2s6Ly8vZXh0ZXJuYWwge1wiY29tbW9uanNcIjpcImZ1c2UuanNcIixcImNvbW1vbmpzMlwiOlwiZnVzZS5qc1wiLFwiYW1kXCI6XCJmdXNlLmpzXCIsXCJyb290XCI6XCJGdXNlXCJ9Iiwid2VicGFjazovLy8uL3NyYy9pbmRleC5qcyJdLCJuYW1lcyI6WyJzaW1wbGVTZXJpYWxpemVyIiwiY29udGVudCIsImVsIiwidGV4dENvbnRlbnQiLCJyZXBsYWNlIiwiZGVmYXVsdEZ1c2VDb25maWciLCJzaG91bGRTb3J0Iiwia2V5cyIsIkRPTUZpbHRlciIsIml0ZW1zIiwiZnVzZUNvbmZpZyIsInNlcmlhbGl6ZXIiLCJpdGVtTWF0Y2hDbGFzcyIsIml0ZW1Ob3RNYXRjaENsYXNzIiwiY29udGFpbmVyTWF0Y2hDbGFzcyIsImNvbnRhaW5lck5vdE1hdGNoQ2xhc3MiLCJwYXJlbnROb2RlIiwic2VyaWFsaXplZEl0ZW1zIiwic2VyaWFsaXplIiwiZnVzZSIsIm1hcCIsImkiLCJpbmRleCIsImZpbHRlciIsInF1ZXJ5Iiwic2hvdWxkRmlsdGVyIiwibGVuZ3RoIiwiZm9yRWFjaCIsIml0ZW0iLCJjbGFzc0xpc3QiLCJhZGQiLCJyZW1vdmUiLCJhcHBlbmRDaGlsZCIsInJlc3VsdHMiLCJzZWFyY2giLCJyZXZlcnNlIiwicmVzdWx0IiwiaW5zZXJ0QmVmb3JlIiwiZmlyc3RDaGlsZCJdLCJtYXBwaW5ncyI6IkFBQUE7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0EsQ0FBQztBQUNELE87QUNWQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7QUFFQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQTtBQUNBOzs7QUFHQTtBQUNBOztBQUVBO0FBQ0E7O0FBRUE7QUFDQSxtREFBMkMsY0FBYzs7QUFFekQ7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQSxhQUFLO0FBQ0w7QUFDQTs7QUFFQTtBQUNBO0FBQ0E7QUFDQSxtQ0FBMkIsMEJBQTBCLEVBQUU7QUFDdkQseUNBQWlDLGVBQWU7QUFDaEQ7QUFDQTtBQUNBOztBQUVBO0FBQ0EsOERBQXNELCtEQUErRDs7QUFFckg7QUFDQTs7QUFFQTtBQUNBOzs7Ozs7O0FDaEVBLCtDOzs7Ozs7Ozs7Ozs7Ozs7O0FDQUE7O0FBRUE7Ozs7O0FBS0EsSUFBTUEsbUJBQW1CLFNBQW5CQSxnQkFBbUI7QUFBQSxRQUFPLEVBQUVDLFNBQVNDLEdBQUdDLFdBQUgsQ0FBZUMsT0FBZixDQUF1QixTQUF2QixFQUFrQyxHQUFsQyxDQUFYLEVBQVA7QUFBQSxDQUF6Qjs7QUFFQTs7OztBQUlBLElBQU1DLG9CQUFvQjtBQUN6QkMsYUFBWSxJQURhO0FBRXpCQyxPQUFNLENBQUMsU0FBRDtBQUZtQixDQUExQjs7SUFLcUJDLFM7O0FBRXBCOzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQW9CQSx5QkFRRztBQUFBLEtBUEZDLEtBT0UsUUFQRkEsS0FPRTtBQUFBLDRCQU5GQyxVQU1FO0FBQUEsS0FORkEsVUFNRSxtQ0FOV0wsaUJBTVg7QUFBQSw0QkFMRk0sVUFLRTtBQUFBLEtBTEZBLFVBS0UsbUNBTFdYLGdCQUtYO0FBQUEsZ0NBSkZZLGNBSUU7QUFBQSxLQUpGQSxjQUlFLHVDQUplLFVBSWY7QUFBQSxrQ0FIRkMsaUJBR0U7QUFBQSxLQUhGQSxpQkFHRSx5Q0FIa0IsY0FHbEI7QUFBQSxrQ0FGRkMsbUJBRUU7QUFBQSxLQUZGQSxtQkFFRSx5Q0FGb0IsV0FFcEI7QUFBQSxrQ0FERkMsc0JBQ0U7QUFBQSxLQURGQSxzQkFDRSx5Q0FEdUIsY0FDdkI7O0FBQUE7O0FBQUE7O0FBQ0YsTUFBS04sS0FBTCxnQ0FBaUJBLEtBQWpCLEdBREUsQ0FDdUI7QUFDekIsTUFBS0UsVUFBTCxHQUFrQkEsVUFBbEI7QUFDQSxNQUFLRCxVQUFMLEdBQWtCQSxVQUFsQjs7QUFFQSxNQUFLRSxjQUFMLEdBQXNCQSxjQUF0QjtBQUNBLE1BQUtDLGlCQUFMLEdBQXlCQSxpQkFBekI7QUFDQSxNQUFLQyxtQkFBTCxHQUEyQkEsbUJBQTNCO0FBQ0EsTUFBS0Msc0JBQUwsR0FBOEJBLHNCQUE5Qjs7QUFFQSxNQUFLQyxVQUFMLEdBQWtCLEtBQUtQLEtBQUwsQ0FBVyxDQUFYLEVBQWNPLFVBQWhDO0FBQ0EsTUFBS0MsZUFBTCxHQUF1QixLQUFLQyxTQUFMLEVBQXZCO0FBQ0EsTUFBS0MsSUFBTCxHQUFZLElBQUksK0NBQUosQ0FBUyxLQUFLRixlQUFkLEVBQStCLEtBQUtQLFVBQXBDLENBQVo7O0FBRUEsUUFBTyxJQUFQO0FBQ0E7O0FBRUQ7Ozs7Ozs7O0FBV0E7Ozs7Ozs7Ozs7Ozs7TUFMQVEsUyxHQUFZO0FBQUEsU0FBTSxNQUFLVCxLQUFMLENBQVdXLEdBQVgsQ0FBZSxVQUFDbEIsRUFBRCxFQUFLbUIsQ0FBTDtBQUFBO0FBQ2hDQyxXQUFPRDtBQUR5QixNQUU3QixNQUFLVixVQUFMLENBQWdCVCxFQUFoQixDQUY2QjtBQUFBLEdBQWYsQ0FBTjtBQUFBLEU7O01BYVpxQixNLEdBQVMsVUFBQ0MsS0FBRCxFQUFXO0FBQ25CLE1BQU1DLGVBQWVELE1BQU1FLE1BQU4sR0FBZSxDQUFwQztBQURtQixNQUdsQmpCLEtBSGtCLFNBR2xCQSxLQUhrQjtBQUFBLE1BR1hPLFVBSFcsU0FHWEEsVUFIVztBQUFBLE1BR0NKLGNBSEQsU0FHQ0EsY0FIRDtBQUFBLE1BR2lCQyxpQkFIakIsU0FHaUJBLGlCQUhqQjtBQUFBLE1BR29DQyxtQkFIcEMsU0FHb0NBLG1CQUhwQztBQUFBLE1BSWxCQyxzQkFKa0IsU0FJbEJBLHNCQUprQjs7QUFPbkI7O0FBQ0FOLFFBQU1rQixPQUFOLENBQWMsZ0JBQVE7QUFDckIsT0FBSUYsWUFBSixFQUFrQjtBQUNqQkcsU0FBS0MsU0FBTCxDQUFlQyxHQUFmLENBQW1CakIsaUJBQW5CO0FBQ0FlLFNBQUtDLFNBQUwsQ0FBZUUsTUFBZixDQUFzQm5CLGNBQXRCO0FBQ0EsSUFIRCxNQUdPO0FBQ05nQixTQUFLQyxTQUFMLENBQWVDLEdBQWYsQ0FBbUJsQixjQUFuQjtBQUNBZ0IsU0FBS0MsU0FBTCxDQUFlRSxNQUFmLENBQXNCbEIsaUJBQXRCO0FBQ0FHLGVBQVdhLFNBQVgsQ0FBcUJDLEdBQXJCLENBQXlCaEIsbUJBQXpCO0FBQ0FFLGVBQVdhLFNBQVgsQ0FBcUJFLE1BQXJCLENBQTRCaEIsc0JBQTVCO0FBQ0E7O0FBRUQ7QUFDQUMsY0FBV2dCLFdBQVgsQ0FBdUJKLElBQXZCO0FBQ0EsR0FiRDs7QUFlQTtBQUNBLE1BQUksQ0FBQ0gsWUFBTCxFQUFtQixPQUFPaEIsTUFBTWlCLE1BQWI7O0FBRW5CO0FBQ0EsTUFBTU8sVUFBVSxNQUFLZCxJQUFMLENBQVVlLE1BQVYsQ0FBaUJWLEtBQWpCLEVBQXdCVyxPQUF4QixFQUFoQjtBQUNBRixVQUFRTixPQUFSLENBQWdCLGtCQUFVO0FBQ3pCLE9BQU1DLE9BQU9uQixNQUFNMkIsT0FBT2QsS0FBYixDQUFiOztBQUVBO0FBQ0FNLFFBQUtDLFNBQUwsQ0FBZUMsR0FBZixDQUFtQmxCLGNBQW5CO0FBQ0FnQixRQUFLQyxTQUFMLENBQWVFLE1BQWYsQ0FBc0JsQixpQkFBdEI7O0FBRUE7QUFDQUcsY0FBV3FCLFlBQVgsQ0FBd0JULElBQXhCLEVBQThCWixXQUFXc0IsVUFBekM7QUFDQSxHQVREOztBQVdBLE1BQUlMLFFBQVFQLE1BQVIsR0FBaUIsQ0FBckIsRUFBd0I7QUFDdkJWLGNBQVdhLFNBQVgsQ0FBcUJDLEdBQXJCLENBQXlCaEIsbUJBQXpCO0FBQ0FFLGNBQVdhLFNBQVgsQ0FBcUJFLE1BQXJCLENBQTRCaEIsc0JBQTVCO0FBQ0EsR0FIRCxNQUdPO0FBQ05DLGNBQVdhLFNBQVgsQ0FBcUJDLEdBQXJCLENBQXlCZixzQkFBekI7QUFDQUMsY0FBV2EsU0FBWCxDQUFxQkUsTUFBckIsQ0FBNEJqQixtQkFBNUI7QUFDQTs7QUFFRCxTQUFPbUIsUUFBUVAsTUFBZjtBQUNBLEU7OzsrREFsSG1CbEIsUyIsImZpbGUiOiJkb20tZmlsdGVyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiKGZ1bmN0aW9uIHdlYnBhY2tVbml2ZXJzYWxNb2R1bGVEZWZpbml0aW9uKHJvb3QsIGZhY3RvcnkpIHtcblx0aWYodHlwZW9mIGV4cG9ydHMgPT09ICdvYmplY3QnICYmIHR5cGVvZiBtb2R1bGUgPT09ICdvYmplY3QnKVxuXHRcdG1vZHVsZS5leHBvcnRzID0gZmFjdG9yeShyZXF1aXJlKFwiZnVzZS5qc1wiKSk7XG5cdGVsc2UgaWYodHlwZW9mIGRlZmluZSA9PT0gJ2Z1bmN0aW9uJyAmJiBkZWZpbmUuYW1kKVxuXHRcdGRlZmluZShbXCJmdXNlLmpzXCJdLCBmYWN0b3J5KTtcblx0ZWxzZSBpZih0eXBlb2YgZXhwb3J0cyA9PT0gJ29iamVjdCcpXG5cdFx0ZXhwb3J0c1tcIkRPTUZpbHRlclwiXSA9IGZhY3RvcnkocmVxdWlyZShcImZ1c2UuanNcIikpO1xuXHRlbHNlXG5cdFx0cm9vdFtcIkRPTUZpbHRlclwiXSA9IGZhY3Rvcnkocm9vdFtcIkZ1c2VcIl0pO1xufSkodGhpcywgZnVuY3Rpb24oX19XRUJQQUNLX0VYVEVSTkFMX01PRFVMRV8wX18pIHtcbnJldHVybiBcblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay91bml2ZXJzYWxNb2R1bGVEZWZpbml0aW9uIiwiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pXG4gXHRcdFx0cmV0dXJuIGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdLmV4cG9ydHM7XG5cbiBcdFx0Ly8gQ3JlYXRlIGEgbmV3IG1vZHVsZSAoYW5kIHB1dCBpdCBpbnRvIHRoZSBjYWNoZSlcbiBcdFx0dmFyIG1vZHVsZSA9IGluc3RhbGxlZE1vZHVsZXNbbW9kdWxlSWRdID0ge1xuIFx0XHRcdGk6IG1vZHVsZUlkLFxuIFx0XHRcdGw6IGZhbHNlLFxuIFx0XHRcdGV4cG9ydHM6IHt9XG4gXHRcdH07XG5cbiBcdFx0Ly8gRXhlY3V0ZSB0aGUgbW9kdWxlIGZ1bmN0aW9uXG4gXHRcdG1vZHVsZXNbbW9kdWxlSWRdLmNhbGwobW9kdWxlLmV4cG9ydHMsIG1vZHVsZSwgbW9kdWxlLmV4cG9ydHMsIF9fd2VicGFja19yZXF1aXJlX18pO1xuXG4gXHRcdC8vIEZsYWcgdGhlIG1vZHVsZSBhcyBsb2FkZWRcbiBcdFx0bW9kdWxlLmwgPSB0cnVlO1xuXG4gXHRcdC8vIFJldHVybiB0aGUgZXhwb3J0cyBvZiB0aGUgbW9kdWxlXG4gXHRcdHJldHVybiBtb2R1bGUuZXhwb3J0cztcbiBcdH1cblxuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZXMgb2JqZWN0IChfX3dlYnBhY2tfbW9kdWxlc19fKVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5tID0gbW9kdWxlcztcblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGUgY2FjaGVcbiBcdF9fd2VicGFja19yZXF1aXJlX18uYyA9IGluc3RhbGxlZE1vZHVsZXM7XG5cbiBcdC8vIGlkZW50aXR5IGZ1bmN0aW9uIGZvciBjYWxsaW5nIGhhcm1vbnkgaW1wb3J0cyB3aXRoIHRoZSBjb3JyZWN0IGNvbnRleHRcbiBcdF9fd2VicGFja19yZXF1aXJlX18uaSA9IGZ1bmN0aW9uKHZhbHVlKSB7IHJldHVybiB2YWx1ZTsgfTtcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7XG4gXHRcdFx0XHRjb25maWd1cmFibGU6IGZhbHNlLFxuIFx0XHRcdFx0ZW51bWVyYWJsZTogdHJ1ZSxcbiBcdFx0XHRcdGdldDogZ2V0dGVyXG4gXHRcdFx0fSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGdldERlZmF1bHRFeHBvcnQgZnVuY3Rpb24gZm9yIGNvbXBhdGliaWxpdHkgd2l0aCBub24taGFybW9ueSBtb2R1bGVzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm4gPSBmdW5jdGlvbihtb2R1bGUpIHtcbiBcdFx0dmFyIGdldHRlciA9IG1vZHVsZSAmJiBtb2R1bGUuX19lc01vZHVsZSA/XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0RGVmYXVsdCgpIHsgcmV0dXJuIG1vZHVsZVsnZGVmYXVsdCddOyB9IDpcbiBcdFx0XHRmdW5jdGlvbiBnZXRNb2R1bGVFeHBvcnRzKCkgeyByZXR1cm4gbW9kdWxlOyB9O1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQoZ2V0dGVyLCAnYScsIGdldHRlcik7XG4gXHRcdHJldHVybiBnZXR0ZXI7XG4gXHR9O1xuXG4gXHQvLyBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGxcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubyA9IGZ1bmN0aW9uKG9iamVjdCwgcHJvcGVydHkpIHsgcmV0dXJuIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbChvYmplY3QsIHByb3BlcnR5KTsgfTtcblxuIFx0Ly8gX193ZWJwYWNrX3B1YmxpY19wYXRoX19cbiBcdF9fd2VicGFja19yZXF1aXJlX18ucCA9IFwiXCI7XG5cbiBcdC8vIExvYWQgZW50cnkgbW9kdWxlIGFuZCByZXR1cm4gZXhwb3J0c1xuIFx0cmV0dXJuIF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gMSk7XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gd2VicGFjay9ib290c3RyYXAgNGNhN2VjYTI5MTRhYWY4YTdmNjIiLCJtb2R1bGUuZXhwb3J0cyA9IF9fV0VCUEFDS19FWFRFUk5BTF9NT0RVTEVfMF9fO1xuXG5cbi8vLy8vLy8vLy8vLy8vLy8vL1xuLy8gV0VCUEFDSyBGT09URVJcbi8vIGV4dGVybmFsIHtcImNvbW1vbmpzXCI6XCJmdXNlLmpzXCIsXCJjb21tb25qczJcIjpcImZ1c2UuanNcIixcImFtZFwiOlwiZnVzZS5qc1wiLFwicm9vdFwiOlwiRnVzZVwifVxuLy8gbW9kdWxlIGlkID0gMFxuLy8gbW9kdWxlIGNodW5rcyA9IDAiLCJpbXBvcnQgRnVzZSBmcm9tICdmdXNlLmpzJztcblxuLyoqXG4gKiBDb252ZXJ0cyBhIERPTSBub2RlIGludG8gYW4gT2JqZWN0XG4gKiBAcGFyYW0gIHtIVE1MRWxlbWVudH0gZWwgVGhlIERPTSBlbGVtZW50IHRvIGJlIHNlcmlhbGl6ZWRcbiAqIEByZXR1cm4ge09iamVjdH0gICAgICAgICBDb250YWlucyBhIHNpbmdsZSAnY29udGVudCcga2V5IHdpdGggdGhlIERPTSB0ZXh0XG4gKi9cbmNvbnN0IHNpbXBsZVNlcmlhbGl6ZXIgPSBlbCA9PiAoeyBjb250ZW50OiBlbC50ZXh0Q29udGVudC5yZXBsYWNlKC9cXHN7Mix9L2csICcgJykgfSk7XG5cbi8qKlxuICogQ29uZmlndXJlcyBGdXNlIHRvIHNlYXJjaCBmb3IgYSAnY29udGVudCcga2V5LCBhcyByZXR1cm5lZCBieSBzaW1wbGVTZXJpYWxpemVyXG4gKiBAdHlwZSB7T2JqZWN0fVxuICovXG5jb25zdCBkZWZhdWx0RnVzZUNvbmZpZyA9IHtcblx0c2hvdWxkU29ydDogdHJ1ZSxcblx0a2V5czogWydjb250ZW50J10sXG59O1xuXG5leHBvcnQgZGVmYXVsdCBjbGFzcyBET01GaWx0ZXIge1xuXG5cdC8qKlxuXHQgKiBDb25maWd1cmUgRE9NRmlsdGVyXG5cdCAqXG5cdCAqIEBwYXJhbSAge05vZGVMaXN0fSBvcHRpb25zLml0ZW1zICAgICAgICBET00gZWxlbWVudHMgdG8gYmUgZmlsdGVyZWRcblx0ICogQHBhcmFtICB7T2JqZWN0fSBbb3B0aW9ucy5mdXNlQ29uZmlnXSAgIENvbmZpZyBvYmplY3QgcGFzc2VkIGRpcmVjdGx5IHRvIGZ1c2UuanNcblx0ICogQHBhcmFtICB7RnVuY3Rpb259IFtvcHRpb25zLnNlcmlhbGl6ZXJdXG5cdCAqICAgICAgICAgU2VyaWFsaXplciBmdW5jdGlvbi4gTXVzdCB0YWtlIGEgRE9NIG5vZGUgYXMgc2luZ2xlIHBhcmFtZXRlciBhbmQgcmV0dXJuXG5cdCAqICAgICAgICAgYW4gT2JqZWN0IGFzIHRoZSBzZXJpYWxpemVkIHJlcHJlc2VudGF0aW9uIG9mIHRoZSBET00gbm9kZS5cblx0ICpcblx0ICogQHBhcmFtICB7U3RyaW5nfSBbb3B0aW9ucy5pdGVtTWF0Y2hDbGFzc11cblx0ICogICAgICAgICBDbGFzcyBhcHBsaWVkIHRvIG1hdGNoaW5nIGl0ZW1zXG5cdCAqIEBwYXJhbSAge1N0cmluZ30gW29wdGlvbnMuaXRlbU5vdE1hdGNoQ2xhc3NdXG5cdCAqICAgICAgICAgQ2xhc3MgYXBwbGllZCB0byBub24tbWF0Y2hpbmcgaXRlbXNcblx0ICogQHBhcmFtICB7U3RyaW5nfSBbb3B0aW9ucy5jb250YWluZXJNYXRjaENsYXNzXVxuXHQgKiAgICAgICAgIENsYXNzIGFwcGxpZWQgdG8gdGhlIHBhcmVudCB3aGVuIGF0IGxlYXN0IG9uZSBtYXRjaCBleGlzdHNcblx0ICogQHBhcmFtICB7U3RyaW5nfSBbb3B0aW9ucy5jb250YWluZXJOb3RNYXRjaENsYXNzXVxuXHQgKiAgICAgICAgIENsYXNzIGFwcGxpZWQgdG8gdGhlIHBhcmVudCB3aGVuIG5vIG1hdGNoZXMgZXhpc3Rcblx0ICpcblx0ICogQHJldHVybiB7T2JqZWN0fSAgICAgICAgICAgICAgICAgICAgICAgIEEgbmV3IERPTUZpbHRlciBpbnN0YW5jZVxuXHQgKi9cblx0Y29uc3RydWN0b3Ioe1xuXHRcdGl0ZW1zLFxuXHRcdGZ1c2VDb25maWcgPSBkZWZhdWx0RnVzZUNvbmZpZyxcblx0XHRzZXJpYWxpemVyID0gc2ltcGxlU2VyaWFsaXplcixcblx0XHRpdGVtTWF0Y2hDbGFzcyA9ICdpcy1tYXRjaCcsXG5cdFx0aXRlbU5vdE1hdGNoQ2xhc3MgPSAnaXMtbm90LW1hdGNoJyxcblx0XHRjb250YWluZXJNYXRjaENsYXNzID0gJ2hhcy1tYXRjaCcsXG5cdFx0Y29udGFpbmVyTm90TWF0Y2hDbGFzcyA9ICdoYXMtbm8tbWF0Y2gnLFxuXHR9KSB7XG5cdFx0dGhpcy5pdGVtcyA9IFsuLi5pdGVtc107IC8vIENhc3QgdG8gYXJyYXlcblx0XHR0aGlzLnNlcmlhbGl6ZXIgPSBzZXJpYWxpemVyO1xuXHRcdHRoaXMuZnVzZUNvbmZpZyA9IGZ1c2VDb25maWc7XG5cblx0XHR0aGlzLml0ZW1NYXRjaENsYXNzID0gaXRlbU1hdGNoQ2xhc3M7XG5cdFx0dGhpcy5pdGVtTm90TWF0Y2hDbGFzcyA9IGl0ZW1Ob3RNYXRjaENsYXNzO1xuXHRcdHRoaXMuY29udGFpbmVyTWF0Y2hDbGFzcyA9IGNvbnRhaW5lck1hdGNoQ2xhc3M7XG5cdFx0dGhpcy5jb250YWluZXJOb3RNYXRjaENsYXNzID0gY29udGFpbmVyTm90TWF0Y2hDbGFzcztcblxuXHRcdHRoaXMucGFyZW50Tm9kZSA9IHRoaXMuaXRlbXNbMF0ucGFyZW50Tm9kZTtcblx0XHR0aGlzLnNlcmlhbGl6ZWRJdGVtcyA9IHRoaXMuc2VyaWFsaXplKCk7XG5cdFx0dGhpcy5mdXNlID0gbmV3IEZ1c2UodGhpcy5zZXJpYWxpemVkSXRlbXMsIHRoaXMuZnVzZUNvbmZpZyk7XG5cblx0XHRyZXR1cm4gdGhpcztcblx0fVxuXG5cdC8qKlxuXHQgKiBBcHBseSB0aGUgc2VyaWFsaXplciBmdW5jdGlvbiBvbiBlYWNoIGl0ZW0uIEVhY2ggc2VyaWFsaXplZCBPYmplY3Qgd2lsbCBoYXZlIGFuXG5cdCAqICdpbmRleCcga2V5IHdpdGggaXRzIG9yaWdpbmFsIERPTSBwb3NpdGlvbiBwbHVzIGFueSBrZXlzIGZvdW5kIGluIHRoZSBPYmplY3Rcblx0ICogcmV0dXJuZWQgYnkgdGhlIHNlcmlhbGl6ZXIgZnVuY3Rpb24uXG5cdCAqIEByZXR1cm4ge0FycmF5fSAgICAgICAgICAgICAgIEFycmF5IG9mIHNlcmlhbGl6ZWQgT2JqZWN0c1xuXHQgKi9cblx0c2VyaWFsaXplID0gKCkgPT4gdGhpcy5pdGVtcy5tYXAoKGVsLCBpKSA9PiAoe1xuXHRcdGluZGV4OiBpLFxuXHRcdC4uLnRoaXMuc2VyaWFsaXplcihlbCksXG5cdH0pKTtcblxuXHQvKipcblx0ICogU2VhcmNoIGFuZCBmaWx0ZXIgdGhlIERPTSBpdGVtcy5cblx0ICogRmlsdGVyaW5nIHdpbGwgcmUtb3JkZXIgdGhlIERPTSBlbGVtZW50cyBieSByZWxldmFuY2UsIGFuZCBhcHBseSBDU1MgY2xhc3Nlc1xuXHQgKiB0byBpbmRpY2F0ZSB3aGljaCBlbGVtZW50cyBtYXRjaCB0aGUgcXVlcnkuIFRoZSBwYXJlbnQgb2YgdGhlIGl0ZW1zIHdpbGwgYWxzb1xuXHQgKiByZWNlaXZlIGNsYXNzZXMgdG8gaW5kaWNhdGUgaWYgbWF0Y2hpbmcgZWxlbWVudHMgYXJlIHByZXNlbnQgb3Igbm90LlxuXHQgKiBAcGFyYW0gIHtTdHJpbmd9IHF1ZXJ5IFRoZSBzZWFyY2ggcXVlcnlcblx0ICogQHJldHVybiB7TnVtYmVyfSAgICAgICBUaGUgbnVtYmVyIG9mIG1hdGNoaW5nIGVsZW1lbnRzXG5cdCAqL1xuXHRmaWx0ZXIgPSAocXVlcnkpID0+IHtcblx0XHRjb25zdCBzaG91bGRGaWx0ZXIgPSBxdWVyeS5sZW5ndGggPiAwO1xuXHRcdGNvbnN0IHtcblx0XHRcdGl0ZW1zLCBwYXJlbnROb2RlLCBpdGVtTWF0Y2hDbGFzcywgaXRlbU5vdE1hdGNoQ2xhc3MsIGNvbnRhaW5lck1hdGNoQ2xhc3MsXG5cdFx0XHRjb250YWluZXJOb3RNYXRjaENsYXNzLFxuXHRcdH0gPSB0aGlzO1xuXG5cdFx0Ly8gUmVzZXQgYWxsIGl0ZW0gYW5kIGNvbnRhaW5lciBjbGFzc2VzXG5cdFx0aXRlbXMuZm9yRWFjaChpdGVtID0+IHtcblx0XHRcdGlmIChzaG91bGRGaWx0ZXIpIHtcblx0XHRcdFx0aXRlbS5jbGFzc0xpc3QuYWRkKGl0ZW1Ob3RNYXRjaENsYXNzKTtcblx0XHRcdFx0aXRlbS5jbGFzc0xpc3QucmVtb3ZlKGl0ZW1NYXRjaENsYXNzKTtcblx0XHRcdH0gZWxzZSB7XG5cdFx0XHRcdGl0ZW0uY2xhc3NMaXN0LmFkZChpdGVtTWF0Y2hDbGFzcyk7XG5cdFx0XHRcdGl0ZW0uY2xhc3NMaXN0LnJlbW92ZShpdGVtTm90TWF0Y2hDbGFzcyk7XG5cdFx0XHRcdHBhcmVudE5vZGUuY2xhc3NMaXN0LmFkZChjb250YWluZXJNYXRjaENsYXNzKTtcblx0XHRcdFx0cGFyZW50Tm9kZS5jbGFzc0xpc3QucmVtb3ZlKGNvbnRhaW5lck5vdE1hdGNoQ2xhc3MpO1xuXHRcdFx0fVxuXG5cdFx0XHQvLyBBbHNvIHJlc2V0IHRoZSBET00gb3JkZXJcblx0XHRcdHBhcmVudE5vZGUuYXBwZW5kQ2hpbGQoaXRlbSk7XG5cdFx0fSk7XG5cblx0XHQvLyBFeGl0IGVhcmx5IGlmIG5vIGZpbHRlcmluZyBuZWVkcyB0byBoYXBwZW5cblx0XHRpZiAoIXNob3VsZEZpbHRlcikgcmV0dXJuIGl0ZW1zLmxlbmd0aDtcblxuXHRcdC8vIFJldmVyc2UgdGhlIHJlc3VsdHMgc28gRE9NIGluc2VydGlvbiB3b3JrcyBhcyBleHBlY3RlZFxuXHRcdGNvbnN0IHJlc3VsdHMgPSB0aGlzLmZ1c2Uuc2VhcmNoKHF1ZXJ5KS5yZXZlcnNlKCk7XG5cdFx0cmVzdWx0cy5mb3JFYWNoKHJlc3VsdCA9PiB7XG5cdFx0XHRjb25zdCBpdGVtID0gaXRlbXNbcmVzdWx0LmluZGV4XTtcblxuXHRcdFx0Ly8gQXBwbHkgbWF0Y2ggY2xhc3NlcyBvbiB0aGUgcmVzdWx0c1xuXHRcdFx0aXRlbS5jbGFzc0xpc3QuYWRkKGl0ZW1NYXRjaENsYXNzKTtcblx0XHRcdGl0ZW0uY2xhc3NMaXN0LnJlbW92ZShpdGVtTm90TWF0Y2hDbGFzcyk7XG5cblx0XHRcdC8vIE1ha2UgRE9NIG9yZGVyIG1hdGNoIHdpdGggcmVzdWx0IG9yZGVyXG5cdFx0XHRwYXJlbnROb2RlLmluc2VydEJlZm9yZShpdGVtLCBwYXJlbnROb2RlLmZpcnN0Q2hpbGQpO1xuXHRcdH0pO1xuXG5cdFx0aWYgKHJlc3VsdHMubGVuZ3RoID4gMCkge1xuXHRcdFx0cGFyZW50Tm9kZS5jbGFzc0xpc3QuYWRkKGNvbnRhaW5lck1hdGNoQ2xhc3MpO1xuXHRcdFx0cGFyZW50Tm9kZS5jbGFzc0xpc3QucmVtb3ZlKGNvbnRhaW5lck5vdE1hdGNoQ2xhc3MpO1xuXHRcdH0gZWxzZSB7XG5cdFx0XHRwYXJlbnROb2RlLmNsYXNzTGlzdC5hZGQoY29udGFpbmVyTm90TWF0Y2hDbGFzcyk7XG5cdFx0XHRwYXJlbnROb2RlLmNsYXNzTGlzdC5yZW1vdmUoY29udGFpbmVyTWF0Y2hDbGFzcyk7XG5cdFx0fVxuXG5cdFx0cmV0dXJuIHJlc3VsdHMubGVuZ3RoO1xuXHR9XG59XG5cblxuXG4vLyBXRUJQQUNLIEZPT1RFUiAvL1xuLy8gLi9zcmMvaW5kZXguanMiXSwic291cmNlUm9vdCI6IiJ9