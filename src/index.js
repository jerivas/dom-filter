import Fuse from 'fuse.js';

/**
 * Converts a DOM node into an Object
 * @param  {HTMLElement} el The DOM element to be serialized
 * @return {Object}         Contains a single 'content' key with the DOM text
 */
const simpleSerializer = el => ({ content: el.textContent.replace(/\s{2,}/g, ' ') });

/**
 * Configures Fuse to search for a 'content' key, as returned by simpleSerializer
 * @type {Object}
 */
const defaultFuseConfig = {
	shouldSort: true,
	keys: ['content'],
};

export default class DOMFilter {

	/**
	 * Configure DOMFilter
	 *
	 * @param  {NodeList} options.items        DOM elements to be filtered
	 * @param  {Object} [options.fuseConfig]   Config object passed directly to fuse.js
	 * @param  {Function} [options.serializer]
	 *         Serializer function. Must take a DOM node as single parameter and return
	 *         an Object as the serialized representation of the DOM node.
	 *
	 * @param  {String} [options.itemMatchClass]
	 *         Class applied to matching items
	 * @param  {String} [options.itemNotMatchClass]
	 *         Class applied to non-matching items
	 * @param  {String} [options.containerMatchClass]
	 *         Class applied to the parent when at least one match exists
	 * @param  {String} [options.containerNotMatchClass]
	 *         Class applied to the parent when no matches exist
	 *
	 * @return {Object}                        A new DOMFilter instance
	 */
	constructor({
		items,
		fuseConfig = defaultFuseConfig,
		serializer = simpleSerializer,
		itemMatchClass = 'is-match',
		itemNotMatchClass = 'is-not-match',
		containerMatchClass = 'has-match',
		containerNotMatchClass = 'has-no-match',
	}) {
		this.items = [...items]; // Cast to array
		this.serializer = serializer;
		this.fuseConfig = fuseConfig;

		this.itemMatchClass = itemMatchClass;
		this.itemNotMatchClass = itemNotMatchClass;
		this.containerMatchClass = containerMatchClass;
		this.containerNotMatchClass = containerNotMatchClass;

		this.parentNode = this.items[0].parentNode;
		this.serializedItems = this.serialize();
		this.fuse = new Fuse(this.serializedItems, this.fuseConfig);

		return this;
	}

	/**
	 * Apply the serializer function on each item. Each serialized Object will have an
	 * 'index' key with its original DOM position plus any keys found in the Object
	 * returned by the serializer function.
	 * @return {Array}               Array of serialized Objects
	 */
	serialize = () => this.items.map((el, i) => ({
		index: i,
		...this.serializer(el),
	}));

	/**
	 * Search and filter the DOM items.
	 * Filtering will re-order the DOM elements by relevance, and apply CSS classes
	 * to indicate which elements match the query. The parent of the items will also
	 * receive classes to indicate if matching elements are present or not.
	 * @param  {String} query The search query
	 * @return {Number}       The number of matching elements
	 */
	filter = (query) => {
		const shouldFilter = query.length > 0;
		const {
			items, parentNode, itemMatchClass, itemNotMatchClass, containerMatchClass,
			containerNotMatchClass,
		} = this;

		// Reset all item and container classes
		items.forEach(item => {
			if (shouldFilter) {
				item.classList.add(itemNotMatchClass);
				item.classList.remove(itemMatchClass);
			} else {
				item.classList.add(itemMatchClass);
				item.classList.remove(itemNotMatchClass);
				parentNode.classList.add(containerMatchClass);
				parentNode.classList.remove(containerNotMatchClass);
			}

			// Also reset the DOM order
			parentNode.appendChild(item);
		});

		// Exit early if no filtering needs to happen
		if (!shouldFilter) return items.length;

		// Reverse the results so DOM insertion works as expected
		const results = this.fuse.search(query).reverse();
		results.forEach(result => {
			const item = items[result.index];

			// Apply match classes on the results
			item.classList.add(itemMatchClass);
			item.classList.remove(itemNotMatchClass);

			// Make DOM order match with result order
			parentNode.insertBefore(item, parentNode.firstChild);
		});

		if (results.length > 0) {
			parentNode.classList.add(containerMatchClass);
			parentNode.classList.remove(containerNotMatchClass);
		} else {
			parentNode.classList.add(containerNotMatchClass);
			parentNode.classList.remove(containerMatchClass);
		}

		return results.length;
	}
}
